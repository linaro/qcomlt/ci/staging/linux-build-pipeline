import os
import time	
import argparse
import subprocess
from junit_xml import TestSuite, TestCase
from multiprocessing.pool import Pool

args = None

def validate_dt(filepath):
	file = os.path.basename(filepath)
	realpath = os.path.realpath(filepath)
	srcfile = os.path.relpath(filepath, args.dtb_path).replace(".dtb", ".dts")

	print("Validating %s" % file)

	validate_args = ['dt-validate', '-m', '-u', args.bindings_path, '-p', args.schema_path, realpath]

	start = time.time()
	output = subprocess.run(validate_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stderr.decode('utf-8')
	end = time.time()

	compatible = subprocess.run(['fdtget', realpath, '/', 'compatible'], stdout=subprocess.PIPE).stdout.decode('utf-8')
	model = subprocess.run(['fdtget', '-d', 'Unknown', realpath, '/', 'model'], stdout=subprocess.PIPE).stdout.decode('utf-8')

	warnings = 0
	for line in output.splitlines():
		if file in line:
			warnings = warnings + 1

	if warnings > 0:
		status = "%d failures" % warnings
	else:
		status = None

	elapsed = end - start
	tc = TestCase(classname=compatible, name=model, file=srcfile, status=status, elapsed_sec=elapsed, stdout=None)

	if warnings > 0:
		tc.add_error_info(output=output.replace(realpath, srcfile))

	return tc

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-p', '--dtb-path')
	parser.add_argument('-b', '--bindings-path')
	parser.add_argument('-s', '--schema-path')
	parser.add_argument('-x', '--xml')

	args = parser.parse_args()

	with Pool() as pool:
		dtbfiles = []
		for root, subdirs, files in os.walk(args.dtb_path):
			for file in files:
				if not file.endswith(".dtb"):
					continue
				
				dtbfiles.append(os.path.join(root, file))

		tcs = pool.map(validate_dt, dtbfiles)

	ts = TestSuite(name="dtbs_check", test_cases=tcs)

	with open(args.xml, 'w+') as file:
		file.write(TestSuite.to_xml_string([ts]))