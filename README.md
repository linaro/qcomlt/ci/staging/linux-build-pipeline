# Linux Build Pipeline

Pipeline to be referenced by Linux git trees to build pushed tags or branches automatically via TuxSuite service.

Required variables:
TUXSUITE_TOKEN: token to submit build jobs